AperiSite
=========

## Requirements

- [Docker](https://docs.docker.com/engine/installation/);
- [Docker Compose](https://docs.docker.com/compose/install/);
- Less time to prepare environment, more time to compose.

## Download

```bash
git clone https://gitlab.com/AperiKube/compose-aperisite aperisite
pushd aperisite/
git submodule update --init --recursive
git submodule foreach --recursive git checkout master
```

## Prepare containers

Pull containers based on docker-compose.yml:

```bash
docker-compose pull
```

## Start

To get it up, please consider using:

```bash
docker-compose up -d
```

## Browse your applications

When done, turn on your web browser and crawl your web server (e.g., [http://127.0.0.1/](http://127.0.0.1/)).

## Live display of logs

```bash
docker-compose logs --follow
```

## Run command on container

Template:

```bash
docker-compose exec SERVICE COMMAND
```

Example:

```bash
docker-compose exec hugo sh
```

Then you will be able to manage your configuration files, debug daemons and much more...

## Edit Aperi'Site

```bash
pushd data/
```

Commit and push your update:

```bash
git add [file]
git commit -m "description"
git push -u origin master
```

## Update Compose Aperi'Site projet

```bash
popd
git add data/
git commit -m "description"
git push -u origin master
```

You're done!
